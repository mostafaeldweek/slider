const images = [
  "assets/download-1.jpg",
  "assets/download-2.jpg",
  "assets/download-3.jpg",
  "assets/download-4.jpg",
  "assets/download-5.jpg",
  "assets/download-6.jpg",
  "assets/download-7.jpg",
  "assets/download-8.jpg",
  "assets/download-9.jpg",
];
let currentIndex = 0;
let autoplayInterval;

const sliderImage = document.getElementById("slider-image");
let prevBtn = document.getElementById("prev-btn");
let nextBtn = document.getElementById("next-btn");
let autoBtn = document.getElementById("autoplay-btn");
let randomBtn = document.getElementById("random-btn");
function showImage(index) {
  sliderImage.classList.add("fade-out");
  setTimeout(() => {
    sliderImage.src = images[index];
    sliderImage.classList.remove("fade-out");
  }, 500);
}

function prevImage() {
  currentIndex = (currentIndex - 1 + images.length) % images.length;
  showImage(currentIndex);
}

function nextImage() {
  currentIndex = (currentIndex + 1) % images.length;
  showImage(currentIndex);
}

function startAutoplay() {
  autoplayInterval = setInterval(nextImage, 1500);
}

function stopAutoplay() {
  clearInterval(autoplayInterval);
  autoplayInterval = null;
}

function toggleAutoplay() {
  if (!autoplayInterval) {
    startAutoplay();
    autoBtn.innerText = "Stop Auto";
  } else {
    autoBtn.innerText = "Auto play";
    stopAutoplay();
  }
}

function randomImage() {
  const randomIndex = Math.floor(Math.random() * images.length);
  currentIndex = randomIndex;
  showImage(currentIndex);
}

prevBtn.addEventListener("click", prevImage);
nextBtn.addEventListener("click", nextImage);
autoBtn.addEventListener("click", toggleAutoplay);
randomBtn.addEventListener("click", randomImage);
